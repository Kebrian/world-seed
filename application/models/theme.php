<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Theme extends CI_Model {

	function __construct() {
    	parent::__construct();

    	//$this->load->set_theme('default');
    }

    function header() {
    	$this->load->view('header');
    }

    function footer() {
    	$this->load->view('footer');
    }

}